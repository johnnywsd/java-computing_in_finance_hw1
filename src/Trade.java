public class Trade{
    static private String DELIMITER = ",";
    
	private int time;
    private String symble;
    private int quantity;
    private double price;
    
    public void parse(String str){
    	String[] tmp = str.split(DELIMITER);
    	this.time = Integer.parseInt(tmp[0]);
    	this.symble = tmp[1];
    	this.quantity = Integer.parseInt(tmp[2]);
    	this.price = Double.parseDouble(tmp[3]);
    }
	public int getTime() {
		return time;
	}
	@Override
	public String toString() {
		return "Trade [time=" + time + ", symble=" + symble + ", quantity="
				+ quantity + ", price=" + price + "]";
	}
	public void setTime(int time) {
		this.time = time;
	}
	public String getSymble() {
		return symble;
	}
	public void setSymble(String symble) {
		this.symble = symble;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}

}
