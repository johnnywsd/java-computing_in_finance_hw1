import java.util.Observable;
import java.util.Observer;


public class TradeAverageObserver implements Observer {

	private double acc = 0;
	private int accQuantity = 0;
	
	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		 if (arg instanceof Trade){
			 this.cal((Trade)arg);
			 this.showOnScreen();
		 }
	}
	
	private void cal(Trade trade){
		acc += trade.getQuantity() * trade.getPrice();
		accQuantity += trade.getQuantity();
	}
	private void showOnScreen(){
		double average = this.acc / this.accQuantity;
		System.out.println(String.format("Current Average: %9.3f", average));
	}


}
