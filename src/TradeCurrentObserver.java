import java.util.Observable;
import java.util.Observer;



public class TradeCurrentObserver implements Observer {

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		if (arg instanceof Trade){
			this.showOnScreen((Trade)arg);
		}
	}
	private void showOnScreen(Trade trade){
		System.out.println(trade.toString());
	}


}
