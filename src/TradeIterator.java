import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;
import java.util.Scanner;



public class TradeIterator implements Iterator<Trade> {

	/**
	 * @param args
	 */
	private String filePath = "";
	private Scanner sc = null;
	
	public TradeIterator(String filePath) throws FileNotFoundException{
		this.filePath = filePath;
		sc = new Scanner(new FileReader(this.filePath));
	}
	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return sc.hasNextLine();
	}

	@Override
	public Trade next() {
		// TODO Auto-generated method stub
		if(this.hasNext()){
			String aline = sc.nextLine();
			Trade trade = new Trade();
			trade.parse(aline);
			return trade;
		}
		return null;
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
