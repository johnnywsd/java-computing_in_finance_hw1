import java.io.FileNotFoundException;
import java.util.Observable;


public class TradeSubject extends Observable implements Runnable {
	private TradeIterator ti = null;
	public TradeSubject(String filePath) throws FileNotFoundException{
		ti = new TradeIterator(filePath);
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(ti.hasNext()){
			setChanged();
            notifyObservers(ti.next());
		}

	}


}
