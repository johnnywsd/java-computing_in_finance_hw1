import java.io.FileNotFoundException;


public class test {

	/**
	 * @param args
	 */
	private static String filePath = "./caocao.txt";
	private static void test1() throws FileNotFoundException{
		TradeIterator ti = new TradeIterator(filePath);
		while(ti.hasNext()){
			Trade t = ti.next();
			System.out.println(t.toString());
		}
	}
	
	private static void test2() throws FileNotFoundException, InterruptedException {
		// TODO Auto-generated method stub
		System.out.println("Start!");
		 
        // create an event source - reads from stdin
        final TradeSubject ts = new TradeSubject(filePath);
 
        // create an observer
        final TradeCurrentObserver tco = new TradeCurrentObserver();
        final TradeAverageObserver tao = new TradeAverageObserver();
 
        // subscribe the observer to the event source
        ts.addObserver(tco);
        ts.addObserver(tao);
 
        // starts the event thread
        Thread thread = new Thread(ts);
        thread.start();
        thread.join();
	}
	
	public static void main(String[] args) throws FileNotFoundException, InterruptedException {
		// TODO Auto-generated method stub
		test2();
		
		
	}

}
